import QtQuick 2.0

Rectangle {
    width: 100;height: 100
    color: "red"
    border.width: control.activeFocus ? 2 : 1
                border.color: "#888"
    property int animatedValue: 0

    opacity: control.activeFocus ? 1 : 0.5

    SequentialAnimation on animatedValue {

        loops: Animation.Infinite
        PropertyAnimation {to : 150;duration: 1000}
        PropertyAnimation {to: 0;duration:1000}
    }

    Text{
        anchors.centerIn: parent
        text: parent.animatedValue
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pointSize: 16
        color: "black"
    }

    MouseArea {
        opacity: 0.5
        anchors.fill : parent
        onClicked: console.log("Button Clicked !")
    }
}
