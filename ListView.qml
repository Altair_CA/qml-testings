import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

ApplicationWindow {
    visible: true
    visibility: "FullScreen"
    width: Screen.width
    height: Screen.height
    title: qsTr("Hello World")

    GridLayout {
        anchors.fill: parent

        columns: 3
        rows: 1

        Rectangle {
            Layout.column: 0

            Layout.row: 0


            Layout.fillHeight: true
            Layout.fillWidth: true

            color: "red"
        }

        Rectangle {
            Layout.column: 1

            Layout.row: 0


            Layout.fillHeight: true
            Layout.fillWidth: true

            color: "green"
            ListView {
                width: parent.width; height: parent.height
                z: 50
                orientation: ListView.Vertical
                model: ListModel {
                    id:model

                }

                delegate: Rectangle {
                    property int appHeight: parentHeight
                    property int appWidth: parentWidth

                    width: appWidth; height: appHeight/20;
                    border.width: 1

                    color: "lightsteelblue"
                    Text {
                        anchors.centerIn: parent
                        text: name
                    }
                }

                add: Transition {
                    NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 400 }
                    NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 400 }
                }

                displaced: Transition {
                    NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.OutBounce }
                }

                focus: true
                MouseArea{
                    anchors.fill: parent
                    onClicked: model.insert(model.count, { "name": "Item " + model.count,"parentHeight":parent.height,"parentWidth":parent.width })
                }
            }
        }

        Rectangle {
            Layout.column: 2

            Layout.row: 0


            Layout.fillHeight: true
            Layout.fillWidth: true

            color: "blue"
        }
    }
}

