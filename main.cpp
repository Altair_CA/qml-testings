#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <mainswitcher.h>
#include <QThread>
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlEngine engine;
    //QQmlComponent component(&engine,QUrl("qrc:/main.qml"));
    //QObject *object = component.create();

    MainSwitcher switcher;
    switcher.setQmlEngine(&engine);
    switcher.setListView();
    //QQmlApplicationEngine engine;
    //engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
