import QtQuick 2.0
import QtQuick.Window 2.2

Window {
    visible: true
    title: qsTr("Hello World")
    visibility: Window.FullScreen
    Column {
        width: parent.width
        height: parent.height
        Button { width: parent.width; height:parent.height *(1/3); objectName: "btn1"}
        Button { width: parent.width; height: parent.height *(1/3); color: "blue"; objectName: "btn2" }
        Button { width: parent.width; height: parent.height *(1/3); radius: 8; objectName: "btn3" }
    }
}



