#include "mainswitcher.h"

MainSwitcher::MainSwitcher(QObject *parent) : QObject(parent)
{
    this->engine = NULL;
    this->listView = NULL;
    this->mainView = NULL;
}

//MainSwitcher::MainSwitcher(QQmlEngine &engine, QString qmlUrl){
//    this->engine = engine;
//    QQmlComponent component(this->engine,qmlUrl);
//    this->object = component.create();
//}
void MainSwitcher::setQmlEngine(QQmlEngine *engine){
    this->engine = engine;
}
void MainSwitcher::setListView(){
    if(this->listView != NULL){
        //delete this->listView;
        //QQmlComponent com(this->engine);
       // QQmlContext *context = new QQmlContext(qmlContext(this->listView));
        //com.create(context);
        this->listView->setProperty("visible",true);
    }else{
        QQmlComponent component(this->engine,QUrl("qrc:/ListView.qml"));
        this->listView = component.create();
        this->listView->setProperty("visible",false);
        this->setMainView();
    }
}
void MainSwitcher::setMainView(){
    if(this->mainView != NULL){
        //delete this->listView;
        //QQmlContext *context = new QQmlContext(qmlContext(this->mainView));
        //QQmlComponent com(this->engine);
        //com.create(context);
        this->mainView->setProperty("visible",true);
    }else{
        QQmlComponent component(this->engine,QUrl("qrc:/main.qml"));
        this->mainView = component.create();
        this->mainView->setProperty("visible",false);
        this->setListView();
    }
}

