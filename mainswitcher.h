#ifndef MAINSWITCHER_H
#define MAINSWITCHER_H

#include <QObject>
#include <QQmlComponent>
#include <QQmlApplicationEngine>
#include <QQmlContext>

class MainSwitcher : public QObject
{
    Q_OBJECT
public:
    explicit MainSwitcher(QObject *parent = 0);
    //MainSwitcher(QQmlEngine &engine,QUrl qmlUrl);
    void setQmlEngine(QQmlEngine *engine);
    QObject *listView;
    QObject *mainView;
    QQmlEngine *engine;
private:

signals:

public slots:
    void setListView();
    void setMainView();
};

#endif // MAINSWITCHER_H
